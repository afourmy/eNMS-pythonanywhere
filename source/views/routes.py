from base.database import get_obj
from base.models import Log
from base.properties import pretty_names, type_to_public_properties
from collections import OrderedDict
from flask import (
    Blueprint,
    current_app,
    jsonify,
    render_template,
    request,
    session,
)
from flask_login import current_user, login_required
from .forms import GoogleEarthForm, SchedulingForm, ViewOptionsForm
from objects.forms import AddNode, AddLink
from objects.models import Filter, Node, node_subtypes, Link
from os.path import join
from passlib.hash import cisco_type7
from scripts.models import Script
from simplekml import Kml
from .styles import create_styles
from subprocess import Popen
from workflows.models import Workflow
# we use os.system and platform.system => namespace conflict
import os
import platform

blueprint = Blueprint(
    'views_blueprint',
    __name__,
    url_prefix='/views',
    template_folder='templates',
    static_folder='static'
)

styles = create_styles(blueprint.root_path)

## Template rendering


@blueprint.route('/<view_type>_view', methods=['GET', 'POST'])
def view(view_type):
    add_node_form = AddNode(request.form)
    add_link_form = AddLink(request.form)
    all_nodes = Node.choices()
    add_link_form.source.choices = add_link_form.destination.choices = all_nodes
    view_options_form = ViewOptionsForm(request.form)
    google_earth_form = GoogleEarthForm(request.form)
    scheduling_form = SchedulingForm(request.form)
    scheduling_form.scripts.choices = Script.choices()
    scheduling_form.workflows.choices = Workflow.choices()
    labels = {'node': 'name', 'link': 'name'}
    # for the sake of better performances, the view defaults to markercluster
    # if there are more than 2000 nodes
    view = 'leaflet' if len(Node.query.all()) < 2000 else 'markercluster'
    if 'view' in request.form:
        view = request.form['view']
    # we clean the session's selected nodes
    session['selection'] = []
    # name to id
    name_to_id = {node.name: id for id, node in enumerate(Node.query.all())}
    return render_template(
        '{}_view.html'.format(view_type),
        filters=Filter.query.all(),
        view=view,
        scheduling_form=scheduling_form,
        view_options_form=view_options_form,
        google_earth_form=google_earth_form,
        add_node_form=add_node_form,
        add_link_form=add_link_form,
        labels=labels,
        names=pretty_names,
        subtypes=node_subtypes,
        name_to_id=name_to_id,
        node_table={
            obj: OrderedDict([
                (property, getattr(obj, property))
                for property in type_to_public_properties[obj.type]
            ])
            for obj in Node.query.all()
        },
        link_table={
            obj: OrderedDict([
                (property, getattr(obj, property))
                for property in type_to_public_properties[obj.type]
            ])
            for obj in Link.query.all()
        })


## AJAX calls


@blueprint.route('/connect_to_<name>', methods=['POST'])
def putty_connection(name):
    return jsonify({})


@blueprint.route('/export_to_google_earth', methods=['POST'])
def export_to_google_earth():
    return jsonify({})


@blueprint.route('/selection', methods=['POST'])
def selection():
    return jsonify({})


@blueprint.route('/get_logs_<name>', methods=['POST'])
def get_logs(name):
    node = get_obj(Node, name=name)
    node_logs = [l.content for l in Log.query.all() if l.source == node.ip_address]
    return jsonify('\n'.join(node_logs))
