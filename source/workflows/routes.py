from base.database import db, get_obj
from base.properties import pretty_names
from flask import Blueprint, jsonify, render_template, request
from flask_login import login_required
from .forms import AddScriptForm, WorkflowCreationForm
from .models import ScriptEdge, Workflow, workflow_factory
from scripts.models import default_scripts, Script
from scripts.routes import type_to_form

blueprint = Blueprint(
    'workflows_blueprint',
    __name__,
    url_prefix='/workflows',
    template_folder='templates',
    static_folder='static'
)

## Template rendering


@blueprint.route('/workflow_management')
def workflows():
    return render_template(
        'workflow_management.html',
        names=pretty_names,
        fields=('name', 'description'),
        workflows=Workflow.query.all(),
        form=WorkflowCreationForm(request.form)
    )


@blueprint.route('/manage_<workflow>', methods=['GET', 'POST'])
def workflow_editor(workflow):
    form = AddScriptForm(request.form)
    form.scripts.choices = [(s[0], s[0]) for s in default_scripts]
    workflow = get_obj(Workflow, name=workflow)
    return render_template(
        'workflow_editor.html',
        type_to_form={t: s(request.form) for t, s in type_to_form.items()},
        form=form,
        names=pretty_names,
        workflow=workflow
    )


## AJAX calls


@blueprint.route('/get_<name>', methods=['POST'])
def get_workflow(name):
    workflow = get_obj(Workflow, name=name)
    properties = ('name', 'description', 'type')
    workflow_properties = {
        property: str(getattr(workflow, property))
        for property in properties
    }
    return jsonify(workflow_properties)


@blueprint.route('/edit_workflow', methods=['POST'])
def edit_workflow():
    return jsonify({})


@blueprint.route('/delete_<name>', methods=['POST'])
def delete_workflow(name):
    return jsonify({})


@blueprint.route('/save_<workflow>', methods=['POST'])
def save_workflow(workflow):
    return jsonify({})
