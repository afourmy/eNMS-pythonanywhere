from base.database import db, get_obj
from base.helpers import str_dict
from difflib import SequenceMatcher
from flask import Blueprint, jsonify, render_template, request, session
from flask_login import current_user, login_required
from .forms import CompareForm
from .models import Task, task_factory
from objects.models import Node
from scripts.models import Script
from re import search, sub
from views.forms import SchedulingForm
from workflows.models import Workflow

blueprint = Blueprint(
    'tasks_blueprint',
    __name__,
    url_prefix='/tasks',
    template_folder='templates',
    static_folder='static'
)

## Template rendering


@blueprint.route('/task_management')
def task_management():
    scheduling_form = SchedulingForm(request.form)
    scheduling_form.scripts.choices = Script.choices()
    scheduling_form.workflows.choices = Workflow.choices()
    tasks = Task.query.all()
    return render_template(
        'task_management.html',
        tasks=tasks,
        compare_form=CompareForm(request.form),
        scheduling_form=scheduling_form
    )


@blueprint.route('/calendar')
def calendar():
    scheduling_form = SchedulingForm(request.form)
    scheduling_form.scripts.choices = Script.choices()
    scheduling_form.workflows.choices = Workflow.choices()
    tasks = {}
    for task in Task.query.all():
        # javascript dates range from 0 to 11, we must account for that by
        # substracting 1 to the month for the date to be properly displayed in
        # the calendar
        python_month = search(r'.*-(\d{2})-.*', task.start_date).group(1)
        month = '{:02}'.format((int(python_month) - 1) % 12)
        tasks[task] = sub(
            r"(\d+)-(\d+)-(\d+) (\d+):(\d+).*",
            r"\1, " + month + r", \3, \4, \5",
            task.start_date
        )
    return render_template(
        'calendar.html',
        tasks=tasks,
        scheduling_form=scheduling_form
    )


## AJAX calls


@blueprint.route('/scheduler', methods=['POST'])
def scheduler():
    return jsonify({})


@blueprint.route('/get/<name>', methods=['POST'])
def get_task(name):
    task = get_obj(Task, name=name)
    task_properties = {
        property: str(getattr(task, property))
        for property in Task.properties
    }
    # prepare the data for javascript
    for p in ('scripts', 'workflows'):
        task_properties[p] = task_properties[p].replace(', ', ',')[1:-1].split(',')
    return jsonify(task_properties)


@blueprint.route('/show_logs/<name>', methods=['POST'])
def show_logs(name):
    task = get_obj(Task, name=name)
    return jsonify(str_dict(task.logs))


@blueprint.route('/get_diff/<name>/<v1>/<v2>/<n1>/<n2>/<s1>/<s2>', methods=['POST'])
def get_diff(name, v1, v2, n1, n2, s1, s2):
    task = get_obj(Task, name=name)
    first = str_dict(task.logs[v1][s1][n1]).splitlines()
    second = str_dict(task.logs[v2][s2][n2]).splitlines()
    opcodes = SequenceMatcher(None, first, second).get_opcodes()
    return jsonify({
        'first': first,
        'second': second,
        'opcodes': opcodes,
    })


@blueprint.route('/compare_logs/<name>', methods=['POST'])
def compare_logs(name):
    task = get_obj(Task, name=name)
    results = {
        'nodes': [node.name for node in task.nodes],
        'scripts': [script.name for script in task.scripts],
        'versions': list(task.logs)
    }
    return jsonify(results)


@blueprint.route('/delete_task/<name>', methods=['POST'])
def delete_task(name):
    return task_management()


@blueprint.route('/pause_task/<name>', methods=['POST'])
def pause_task(name):
    return task_management()


@blueprint.route('/resume_task/<name>', methods=['POST'])
def resume_task(name):
    return task_management()
