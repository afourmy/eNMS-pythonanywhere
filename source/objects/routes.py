from base.database import db, get_obj
from base.helpers import allowed_file
from base.properties import (
    pretty_names,
    link_public_properties,
    node_public_properties
)
from flask import Blueprint, jsonify, render_template, request
from flask_login import login_required
from .forms import AddNode, AddLink, FilteringForm
from .models import filter_factory, Filter, Link, Node, object_class, object_factory
from werkzeug.utils import secure_filename
from xlrd import open_workbook
from xlrd.biffh import XLRDError

blueprint = Blueprint(
    'objects_blueprint',
    __name__,
    url_prefix='/objects',
    template_folder='templates',
    static_folder='static'
)

## Template rendering


@blueprint.route('/object_management', methods=['GET', 'POST'])
def objects():
    add_node_form = AddNode(request.form)
    add_link_form = AddLink(request.form)
    all_nodes = Node.choices()
    add_link_form.source.choices = add_link_form.destination.choices = all_nodes
    return render_template(
        'object_management.html',
        names=pretty_names,
        node_fields=node_public_properties,
        nodes=Node.visible_objects(),
        link_fields=link_public_properties,
        links=Link.visible_objects(),
        add_node_form=add_node_form,
        add_link_form=add_link_form
    )


@blueprint.route('/object_filtering')
def filter_objects():
    return render_template(
        'object_filtering.html',
        form=FilteringForm(request.form),
        names=pretty_names,
        filters=[f.name for f in Filter.query.all()]
    )


## AJAX calls


@blueprint.route('/get/<obj_type>/<name>', methods=['POST'])
def get_object(obj_type, name):
    cls = Node if obj_type == 'node' else Link
    properties = node_public_properties if cls == Node else link_public_properties
    obj = get_obj(cls, name=name)
    obj_properties = {
        property: str(getattr(obj, property))
        for property in properties
    }
    return jsonify(obj_properties)


@blueprint.route('/edit_object', methods=['GET', 'POST'])
def edit_object():
    return jsonify({})


@blueprint.route('/delete/<obj_type>/<name>', methods=['POST'])
def delete_object(obj_type, name):
    return jsonify({})


@blueprint.route('/process_filter', methods=['POST'])
def process_filter():
    return jsonify('update')


@blueprint.route('/get/<name>', methods=['POST'])
def get_filter(name):
    return jsonify(get_obj(Filter, name=name).get_properties())


@blueprint.route('/<name>/filter_objects', methods=['POST'])
def get_filtered_objects(name):
    filter = get_obj(Filter, name=name)
    objects = filter.filter_objects()
    return jsonify(objects)
